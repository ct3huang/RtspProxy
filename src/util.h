/*
 * =====================================================================================
 *
 *       Filename:  util.h
 *
 *    Description:  This file defines useful functions for the program
 *
 *        Version:  1.0
 *        Created:  01/09/2017
 *       Revision:  01/10/2017
 *
 *         Author:  Wenfeng LIN
 *          Email:  wf.lin@clobotics.com
 *        Company:  Clobotics
 *
 * =====================================================================================
 */


#ifndef _UTIL_H_
#define _UTIL_H_

#define HOST_NAME_MAX_LENGTH 50

enum {
	FALSE = 0,
	TRUE
};

struct configuration {
	char camera_host[HOST_NAME_MAX_LENGTH];
	int camera_port;
	char cloud_host[HOST_NAME_MAX_LENGTH];
	int cloud_port;
	char sim_host[HOST_NAME_MAX_LENGTH];
	int sim_port;
};

extern int gDebug;
// typedef struct configuration configuration;

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  util_print_help
 *  Description:  Display help message for user
 * =====================================================================================
 *   Parameters:  none
 *      Returns:  none
 * =====================================================================================
 */
void util_print_help (const char *progname);

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  util_parse_arg
 *  Description:  Parse command-line arguments
 * =====================================================================================
 *   Parameters:  same with main()
 *      Returns:  value of type struct configuration
 * =====================================================================================
 */
struct configuration util_parse_arg (int argc, char *argv[]);

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  util_log
 *  Description:  Log information to standard output
 * =====================================================================================
 *   Parameters:  pointer to msg buffer
 *      Returns:  none
 * =====================================================================================
 */
void util_log(const char *msg);

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  util_error
 *  Description:  Log information to standard error output and terminate program
 * =====================================================================================
 *   Parameters:  pointer to msg buffer
 *      Returns:  none
 * =====================================================================================
 */
void util_error(const char *msg);

#endif
