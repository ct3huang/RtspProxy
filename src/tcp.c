/*
 * =====================================================================================
 *
 *       Filename:  tcp.c
 *
 *    Description:  This file implements the TCP functions
 *
 *        Version:  1.0
 *        Created:  01/09/2017
 *       Revision:  01/11/2017
 *
 *         Author:  Wenfeng LIN
 *          Email:  wf.lin@clobotics.com
 *        Company:  Clobotics
 *
 * =====================================================================================
 */


#include <errno.h>
#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <assert.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include "tcp.h"
#include "util.h"

int tcp_connect(const char *host, int port)
{
	struct hostent *hn;
	struct sockaddr_in sa;
	int sock;
	int conn_stat;

	/* allocate sockets */
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0) {
		util_log("tcp_connect() -> socket() failed");
		return -1;
	}

	/* configure address */
	memset(&sa, 0, sizeof(sa));
	hn = gethostbyname(host);
	if (hn == NULL) {
		util_log("tcp_connect() -> gethostbyname() failed");
		return -1;
	}
	memcpy(	
		(char *)&sa.sin_addr.s_addr,
		(char *)hn->h_addr, 
		hn->h_length
	     );
	sa.sin_port = htons(port);
	sa.sin_family = AF_INET;

	/* connect */
	conn_stat = connect(
			sock,
			(struct sockaddr *)&sa,
			sizeof(sa)
			);

	/* result */
	if (conn_stat == 0) 
		return sock;
	else
		util_log("tcp_connect -> connect() failed");

	return -1;
}

int tcp_recv(int sock, char *buf, int max_size)
{
	assert(sock >= 0 && buf != NULL && max_size > 0);
	int recv_cnt;
        recv_cnt = recv(sock, buf, max_size, 0);
	if (recv_cnt == 0)
		util_log("tcp_recv() -> recv(): connection terminated");
	if (recv_cnt < 0)
		util_log("tcp_recv() -> recv(): I/O error");
	return recv_cnt;
}

int tcp_send(int sock, char *buf, int size)
{
	assert(sock >= 0 && buf != NULL && size > 0 && size <= TCP_BUF_SIZE_MAX);

	int send_idx;
	int send_cnt;

	send_idx = 0;
	while (send_idx < size) {
		send_cnt = send(sock, &buf[send_idx], size - send_idx, 0);
		if (send_cnt < 0)
			return 0;
		send_idx += send_cnt;
	}
	return 1;
}
