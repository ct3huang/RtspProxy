/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  The program acts as a RTSP proxy
 *
 *        Version:  1.0
 *        Created:  01/09/2017
 *       Revision:  01/11/2017
 *
 *         Author:  Wenfeng LIN
 *          Email:  wf.lin@clobotics.com
 *        Company:  Clobotics
 *
 * =====================================================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/select.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>

#include <assert.h>

#include "util.h"
#include "tcp.h"
#include "rtsp.h"

#define MSG_DIR_CAM_TO_CLD 1
#define MSG_DIR_CLD_TP_CAM 2

char buf_tcp_recv[TCP_BUF_SIZE_MAX];
char buf_tcp_send[TCP_BUF_SIZE_MAX];
char buf_rtsp_cam_to_cld[TCP_BUF_SIZE_MAX * 2];
char buf_rtsp_cld_to_cam[TCP_BUF_SIZE_MAX * 2];

int buf_tcp_recv_len;
int buf_tcp_send_len;
int buf_rtsp_cam_to_cld_len;
int buf_rtsp_cld_to_cam_len;

struct configuration config;
int sock_camera, sock_cloud;

void print_buf(void)
{
	int i;

	if (buf_tcp_recv_len > 0) {
		printf("----- TCP recv -----\n");
		for (i = 0; i < buf_tcp_recv_len; i++) {
			putchar(buf_tcp_recv[i]);
		}
		printf("\n---------------------------\n");
	}

	if (buf_tcp_send_len > 0) {
		printf("----- TCP send -----\n");
		for (i = 0; i < buf_tcp_send_len; i++)
			putchar(buf_tcp_send[i]);
		printf("\n---------------------------\n");
	}

	if (buf_rtsp_cam_to_cld_len > 0) {
		printf("----- RTSP cam to cld -----\n");
		for (i = 0; i < buf_rtsp_cam_to_cld_len; i++)
			putchar(buf_rtsp_cam_to_cld[i]);
		printf("\n---------------------------\n");
	}

	if (buf_rtsp_cld_to_cam_len> 0) {
		printf("----- RTSP cld to cam -----\n");
		for (i = 0; i < buf_rtsp_cld_to_cam_len; i++)
			putchar(buf_rtsp_cld_to_cam[i]);
		printf("\n---------------------------\n");
	}

}

void process_msg (int direction)
{
	assert(direction == MSG_DIR_CAM_TO_CLD || direction == MSG_DIR_CLD_TP_CAM);

	char *buf_rtsp;
	int *buf_rtsp_len;
	int sock_recv, sock_send;

	switch (direction) {
	case MSG_DIR_CAM_TO_CLD:
		buf_rtsp = buf_rtsp_cam_to_cld;
		buf_rtsp_len = &buf_rtsp_cam_to_cld_len;
		sock_recv = sock_camera;
		sock_send = sock_cloud;
		break;
	case MSG_DIR_CLD_TP_CAM:
		buf_rtsp = buf_rtsp_cld_to_cam;
		buf_rtsp_len = &buf_rtsp_cld_to_cam_len;
		sock_recv = sock_cloud;
		sock_send = sock_camera;
		break;
	}

	if (gDebug) fprintf(stdout, "Msg direction: %s\n", (direction == MSG_DIR_CAM_TO_CLD) ? "cam -> cloud" : "cloud -> cam");

	/* recv tcp msg */
	buf_tcp_recv_len = tcp_recv(sock_recv, buf_tcp_recv, sizeof(buf_tcp_recv));
	if (buf_tcp_recv_len <= 0) {
		close(sock_recv);
		close(sock_send);
		util_error("main() -> tcp_recv() failed");
	}

	if (gDebug) fprintf(stdout, "TCP message received\n");
	if (gDebug) print_buf();

	/* if buffer is full, clear the buffer (should not happen for RTSP control message) */
	if (*buf_rtsp_len + buf_tcp_recv_len > TCP_BUF_SIZE_MAX * 2) {
		if (gDebug) fprintf(stdout, "Buffer is full, clearing\n");
		*buf_rtsp_len = 0;
		return;
	}

	/* concat the rest of last TCP msg in front of the new one */ 
	memcpy(&buf_rtsp[*buf_rtsp_len], buf_tcp_recv, buf_tcp_recv_len);
	*buf_rtsp_len += buf_tcp_recv_len;
	buf_tcp_recv_len = 0;

	char *msg_end;
	/* while the buffer contains a whole RTSP message */
	while(*buf_rtsp_len > 0 && (msg_end = rtsp_end_of_msg(buf_rtsp, *buf_rtsp_len)) != NULL) {

		if (gDebug) fprintf(stdout, "New msg\n");

		int go_on;
		char *line_end;
		int msg_len;

		go_on = FALSE;
		msg_len = msg_end - buf_rtsp + 1; // message length
		buf_tcp_send_len = 0; // 0 means nothing to send yet

		/* move the msg to tcp buffer first */
		if (msg_len <= TCP_BUF_SIZE_MAX) {
			memcpy(buf_tcp_send, buf_rtsp, msg_len);
			go_on = TRUE;
		}
		*buf_rtsp_len -= msg_len;
		if (*buf_rtsp_len > 0)
			memmove(buf_rtsp, msg_end + 1, *buf_rtsp_len);

		if (go_on == FALSE) {
			if (gDebug) fprintf(stdout, "Ignore long msg\n");
			continue;
		}

		// parse message
		if (gDebug) fprintf(stdout, "Start to parse msg\n");
		if (gDebug) print_buf();

		struct rtsp_msg msg;
		msg = rtsp_parse(buf_tcp_send, msg_len);
		if (msg.type == RTSP_TYPE_REPLY) {
			if (gDebug) fprintf(stdout, "Msg type: reply\n");
			buf_tcp_send_len = msg_len;
		}
		else if (msg.type ==  RTSP_TYPE_REQUEST) {
			if (gDebug) fprintf(stdout, "Msg type: request\n");
			char url_original[200];
			char url_replaced[200];
			int url_original_len;
			int url_replaced_len;
			int reserved;
			char *url_pos;
			int new_msg_len;
			switch (msg.method) {
			case RTSP_METHOD_OPTIONS:
			case RTSP_METHOD_DESCRIBE:
				if (gDebug) fprintf(stdout, "Msg method: OPTIONS or DESCRIBE\n");
				sprintf(url_original, "%s:%d", config.sim_host, config.sim_port);	
				sprintf(url_replaced, "%s:%d", config.camera_host, config.camera_port);	
				url_original_len = strlen(url_original);
				url_replaced_len = strlen(url_replaced);
				if (gDebug) fprintf(stdout, "Original url -> %s\nNew url -> %s\n", url_original, url_replaced);
				// new msg length
				new_msg_len = msg_len + url_replaced_len - url_original_len;
				if (gDebug) fprintf(stdout, "Message length: %d -> %d\n", msg_len, new_msg_len);
				// find url position 
				line_end = rtsp_end_of_line(buf_tcp_send, msg_len);
				reserved = *(line_end - 1);
				url_pos = strstr(buf_tcp_send, url_original);
				*(line_end - 1) = reserved;
				// if original url exists and new url fits the buffer 
				if (new_msg_len <= TCP_BUF_SIZE_MAX && NULL != url_pos) {
					int rest_len = msg_len - url_original_len - (url_pos - buf_tcp_send);
					memmove(url_pos + url_replaced_len, url_pos + url_original_len, rest_len);	
					memcpy(url_pos, url_replaced, url_replaced_len);
					buf_tcp_send_len = new_msg_len;
					if (gDebug) fprintf(stdout, "Replacing url\n");
				}
				else {
					util_log("process_msg(), cannot replace url");
				}
				break;
			case RTSP_METHOD_TEARDOWN:
			case RTSP_METHOD_GET_PARAMETER:
				if (gDebug) fprintf(stdout, "Msg method: TEARDOWN\n");
				buf_tcp_send_len = msg_len;
				break;
			default: // do not process
				buf_tcp_send_len = msg_len;
				if (gDebug) fprintf(stdout, "Msg method: Unknown\n");
			}
		}

		if (buf_tcp_send_len > 0) {
			if (gDebug) fprintf(stdout, "Sending msg\n");
			if (gDebug) print_buf();
			int res = tcp_send(sock_send, buf_tcp_send, buf_tcp_send_len);
			if (res == 0) {
				close(sock_recv);
				close(sock_send);
				util_error("process_msg() -> tcp_send() failed");
			}
			buf_tcp_send_len = 0;

		}
	} 

	if (gDebug) fprintf(stdout, "Msg process done\n");

}

int main (int argc, char *argv[])
{
	fd_set read_fs;
	int sock_max;

	/* parse arguments */
	config = util_parse_arg(argc, argv);

	/* establish connection with camera and cloud server*/
	sock_camera = tcp_connect(config.camera_host, config.camera_port);
	if (sock_camera == -1)
		util_error("main() -> tcp_connect() failed #1");
	sock_cloud = tcp_connect(config.cloud_host, config.cloud_port);
	if (sock_cloud == -1)
		util_error("main() -> tcp_connect() failed #2");
	sock_max = (sock_camera > sock_cloud) ? sock_camera : sock_cloud;
	sock_max++;
	util_log("Connected");

	/* forward message */
	while (1) {
		FD_ZERO(&read_fs);
		FD_SET(sock_camera, &read_fs);
		FD_SET(sock_cloud, &read_fs);

		// wait for incoming message 
		if (select(sock_max, &read_fs, NULL, NULL, NULL) < 0) 
			util_error("main() -> select() failed");

		if (FD_ISSET(sock_camera, &read_fs)) {
			process_msg(MSG_DIR_CAM_TO_CLD);
		}

		if (FD_ISSET(sock_cloud, &read_fs)) {
			process_msg(MSG_DIR_CLD_TP_CAM);
		}

	}
	return 0;
}
