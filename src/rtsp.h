/*
 * =====================================================================================
 *
 *       Filename:  rtsp.h
 *
 *    Description:  This file defines the RTSP functions
 *
 *        Version:  1.0
 *        Created:  01/10/2017
 *       Revision:  01/11/2017
 *
 *         Author:  Wenfeng LIN
 *          Email:  wf.lin@clobotics.com
 *        Company:  Clobotics
 *
 * =====================================================================================
 */

#ifndef _RTSP_H_
#define _RTSP_H_

#define RTSP_TOKEN "RTSP/1.0"
#define URL_LEN_MAX 200

enum RTSP_METHOD {
	RTSP_METHOD_INVALID = 0,
	RTSP_METHOD_OPTIONS,
	RTSP_METHOD_DESCRIBE,
	RTSP_METHOD_ANNONCE,
	RTSP_METHOD_SETUP,
	RTSP_METHOD_PLAY,
	RTSP_METHOD_PAUSE,
	RTSP_METHOD_TEARDOWN,
	RTSP_METHOD_GET_PARAMETER,
	RTSP_METHOD_SET_PARAMETER,
	RTSP_METHOD_REDIRECT,
	RTSP_METHOD_RECORD,
	RTSP_METHOD_OTHER
};

enum RTSP_TYPE {
	RTSP_TYPE_INVALID = 0,
	RTSP_TYPE_REQUEST,
	RTSP_TYPE_REPLY
};

struct rtsp_msg {
	int length;
	int type;
	int method;
	char url[200];
};

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  rtsp_parse
 *  Description:  Parse RTSP message
 * =====================================================================================
 *   Parameters:  
 *                buf  -> pointer to msg buffer
 *                size -> buffer size
 *      Returns:  
 *                value of type struct rtsp_msg, 
 *                msg.type is set as RTSP_TYPE_INVALID on failure
 * =====================================================================================
 */
struct rtsp_msg rtsp_parse(char *buf, int size);

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  rtsp_end_of_line
 *  Description:  Locate end of line by looking for "\r\n"
 * =====================================================================================
 *   Parameters:  
 *                buf  -> pointer to msg buffer
 *                size -> buffer size
 *      Returns:  
 *                pointer to the last '\n' of the line, NULL on failure
 * =====================================================================================
 */
 char *rtsp_end_of_line(const char *buf, int size);

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  rtsp_end_of_msg
 *  Description:  Locate end of msg
 * =====================================================================================
 *   Parameters:  
 *                buf  -> pointer to msg buffer
 *                size -> buffer size
 *      Returns:  
 *                pointer to the last '\n' of the message, NULL on failure
 * =====================================================================================
 */
 char *rtsp_end_of_msg(char *buf, int size);

#endif
