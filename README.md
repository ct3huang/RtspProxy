RTSP proxy : a simple proxy for RTSP

To test:
	1. open a terminal, run "sudo sh script/azureSim.sh" 
	2. open another terminal, run "sh script/run.sh"
	3. open VLC play stream of url "rtsp://[USERNAME:PASSWORD@IP:HOST]/live"
